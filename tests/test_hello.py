import unittest

from src.my_pkg.hello import get_hello

class TestHello(unittest.TestCase):
    def test_hello_should_return_hello_str(self):
        self.assertEqual(get_hello(2), "hello")